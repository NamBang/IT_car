﻿
#include "function.h"

int check_crossline(void);
int check_rightline(void);
int check_leftline(void);
int check_dunglai(void);

uint8_t flagC = 0, flagL = 0, flagR = 0;
uint8_t  pattern;
uint16_t cnt1, pulse_v;

int main(void)
{
	INIT();
	sel_mode();
	///////////////////////////////////////////////////////////////////////
	while (1)
	{
		if (get_button(BTN1)) break;
		if (get_button(BTN0)) pulse_v = 10;
		led7(pulse_v);
		sensor_cmp(0xff);
	}
	//////////////////////////////////////////////////////////////////////////
	led7(10);
	while (1)
	{
		pattern = 1;
		while (1)
		{
			switch (pattern)
			{
				case 1:
				if(check_dunglai())
				{
					pattern = 211;
					break;
				}
				if (check_crossline()) //Be 90
				{
					flagC = 1;
					pattern = 21;
					break;
				}
				if (check_rightline()) // Chuyen lan phai
				{
					_delay_us(250);  // delay rat rat rat nho
					if((sensor_cmp(0b11111111) == 0b00111111) ||(sensor_cmp(0b11111111)==0b01111111))  //xet tung bit tiep theo co dc bat hay khong
					{
						flagC = 1;
						pattern = 21;
						break;
					}
					else
					{
						flagR = 1;
						pulse_v = 0;
						cnt1 = 0;
						pattern = 51;
						break;
					}
				}
				if (check_leftline())  // ?Chuyen lan trai
				{
					_delay_us(250);  // delay rat rat rat nho
					if((sensor_cmp(0b11111111) == 0b11111100) ||(sensor_cmp(0b11111111)==0b11111110))  //xet tung bit tiep theo co dc bat hay khong
					{
						flagC = 1;
						pattern = 21;
						break;
					}
					else
					{
						flagL = 1;
						pulse_v = 0;
						cnt1 = 0;
						pattern = 61;
						break;
					}
				}

				switch (sensor_cmp(0b01111110))
				{
					case 0b00011000:	// Chay thang
					handle(0);
					speed(60,60);
					break;

					//lech phai
					case 0b00011100:
					case 0b00001000:
					speed(60, 55);
					handle(-10);
					break;

					case 0b00001100:
					speed(60, 40);
					handle(-18);
					break;

					case 0b00001110:
					case 0b00000100:
					speed(60, 35);
					handle(-25);
					break;

					case 0b00000110:
					speed(60, 33);
					handle(-45);
					break;

					case 0b00000010:
					speed(60, 25);
					handle(-60);
					pattern = 11;	//lech phai goc lon
					break;
					//////////////////////////////////////////////////////////////////////////

					//lech trai
					case 0b00111000:
					case 0b00010000:
					speed(50, 60);
					handle(10);
					break;

					case 0b00110000:
					speed(40, 60);
					handle(18);
					break;

					case 0b01110000:
					case 0b00100000:
					speed(25, 60);
					handle(+25);
					break;

					case 0b01100000:
					speed(40, 60);
					handle(+40);
					break;

					case 0b01000000:
					speed(25, 60);
					handle(+60);
					pattern = 12; //lech trai goc lon
					cnt1 = 0;
					pulse_v = 0;
					break;

					case 0b11111111:
					speed(0,0);
					handle(0);
					break;

					default:
					break;
				}
				break;
				case 211:
				{
					switch (sensor_cmp(0b11111111))
					{
						case 0b00000000:
						handle(0);
						speed(0, 0);
						break;
					}
				}
				break;
				case 11:
				led7(11);
				switch (sensor_cmp(0b11111111))
				{
					case 0b00000011:
					handle(-40);
					speed(60, 35);
					break;

					case 0b00000110:
					handle(-35);
					speed(60, 27);
					break;

					case 0b00001100:
					handle(-30);
					speed(60, 35);
					break;

					case 0b00000001:
					handle(-70);
					speed(60, 20);
					break;

					case 0b10000001:
					handle(-70);
					speed(60, 15);
					break;

					case 0b10000000:
					handle(-80);
					speed(60, 10);
					break;

					case 0b11000000:
					handle(-95);
					speed(60, 0);
					break;

					case 0b01100000:
					case 0b11100000:
					handle(-110);
					speed(60, 0);
					break;

					case 0b01110000:
					case 0b11110000:
					handle(-120);
					speed(60, -10);
					break;

					case 0b00010000:
					case 0b00001000:
					case 0b00000100:
					case 0b00011000:
					pattern = 1;
					pulse_v = 0;
					cnt1 = 0;
					led7(10);
					break;

					default:
					break;
				}
				break;

				case 12:
				led7(12);
				switch (sensor_cmp(0b11111111))
				{
					case 0b11000000:
					handle(+40);
					speed(40, 70);
					break;

					case 0b01100000:
					handle(+35);
					speed(35, 60);
					break;

					case 0b00110000:
					handle(+40);
					speed(45, 60);
					break;

					case 0b10000000:
					handle(+80);
					speed(20, 60);
					break;

					case 0b10000001:
					handle(+70);
					speed(15, 70);
					break;

					case 0b00000001:
					handle(+70);
					speed(7, 60);
					break;

					case 0b00000011:
					handle(+70);
					speed(0, 60);
					break;

					case 0b00000110:
					case 0b00000111:
					handle(+110);
					speed(0, 60);
					break;

					case 0b00001110:
					case 0b00001111:
					handle(+110);
					speed(-10, 60);
					break;

					case 0b00001000:
					case 0b00010000:
					case 0b00100000:
					case 0b00011000:
					pattern = 1;
					pulse_v = 0;
					cnt1 = 0;
					led7(10);
					break;

					default:
					break;
				}
				break;

			case 21: // queo 90 do
			led7(21);
			speed(-35,-35);
			_delay_ms(100);
			while(1)
			{
				switch (sensor_cmp(0b11111111))
				{
					case 0b11100000:
					case 0b11110000:
					case 0b11111000:
					handle(850);
					speed(40,60);
					flagC = 0;
					pattern = 1;
					_delay_ms(500);
					break;

					case 0b00000111:
					case 0b00001111:
					case 0b00011111:
					handle(-850);
					speed(60,40);
					flagC = 0;
					pattern = 1;
					_delay_ms(700);
					break;

					case 0b00011000:
					case 0b00111100:
					handle(0);
					speed(20, 20);
					break;

					case 0b00011100:
					case 0b00001000:
					speed(20, 10);
					handle(-15);break;

					case 0b00001100:
					speed(20, 5);
					handle(-23);break;

					case 0b00000110:
					speed(0,20);
					handle(-41);break;

					case 0b00001110:
					case 0b00000100:
					speed(20, 0);
					handle(-36);break;

					case 0b00111000:
					case 0b00010000:
					speed(10, 20);
					handle(15);break;

					case 0b00110000:
					speed(5, 20);
					handle(-23);break;

					case 0b01100000:
					speed(0,20);
					handle(41);break;

					case 0b01110000:
					case 0b00100000:
					speed(0, 20);
					handle(37);break;

					case 0b00000000:
					speed(80,80);
					handle(0);
					_delay_ms(500);
					break;
					pattern = 1;
					pulse_v = 0;
					cnt1 = 0;
					led7(10);
					default:
					break;
				}
				if(!flagC && pattern == 1)
				break;
			}
			break;

			case 51: // chuyen lan phai
			led7(51);
			speed(0,0);
			while (1)
			{
				switch(sensor_cmp(0b11111111))
				{
					case 0b0000000:
					handle(-70);
					speed(30,40);
					flagR = 0;
					pattern = 1;
					break;

					case 0b00011000:
					case 0b00111100:
					handle(0);
					speed(50, 50);
					break;

					case 0b00011100:
					case 0b00001000:
					speed(40, 50);
					handle(-10);break;

					case 0b00001100:
					speed(35, 50);
					handle(-18);break;

					case 0b00000110:
					speed(30,50);
					handle(-36);break;

					case 0b00001110:
					case 0b00000100:
					speed(30, 50);
					handle(-31);break;

					case 0b00111000:
					case 0b00010000:
					speed(50, 40);
					handle(10);break;

					case 0b00110000:
					speed(50, 35);
					handle(18);break;

					case 0b01100000:
					speed(50,30);
					handle(36);break;

					case 0b01110000:
					case 0b00100000:
					speed(50, 30);
					handle(32);break;
					default:
					pattern = 1;
					pulse_v = 0;
					cnt1 = 0;
					led7(10);
					break;
				}
				if (!flagR && pattern == 1)
				{
					speed(10,10);
					break;
				}
			}
			break;

			case 61: // chuyen lan trai
			speed(0,0);
			led7(61);
			while (1)
			{
				switch(sensor_cmp(0b11111111))
				{
					case 0b0000000:
					handle(70);
					speed(40,30);
					flagL = 0;
					pattern = 1;
					break;

					case 0b00011000:
					case 0b00111100:
					handle(0);
					speed(50, 50);
					break;

					case 0b00011100:
					case 0b00001000:
					speed(40, 50);
					handle(-10);break;

					case 0b00001100:
					speed(35, 50);
					handle(-18);break;

					case 0b00000110:
					speed(30,50);
					handle(-36);break;

					case 0b00001110:
					case 0b00000100:
					speed(30, 50);
					handle(-31);break;

					case 0b00111000:
					case 0b00010000:
					speed(50, 40);
					handle(10);break;

					case 0b00110000:
					speed(50, 35);
					handle(18);break;

					case 0b01100000:
					speed(50,30);
					handle(36);break;

					case 0b01110000:
					case 0b00100000:
					speed(50, 30);
					handle(32);break;
					default:
					pattern = 1;
					pulse_v = 0;
					cnt1 = 0;
					led7(10);
					break;
				}
				if (!flagL && pattern == 1)
				{
					speed(10,10);
					break;
				}
			}
			break;

			default:
			pattern = 1;
			break;
		}
	}

}
}

ISR(TIMER0_COMP_vect)
{
	cnt1++;
	print();			//Qu�t LED7 �o?n
}
ISR(INT0_vect)
{
	pulse_v++;
	//pulse_ratio++;
}
int check_crossline(void)
{
	int ret = 0;
	//if ((sensor_cmp(0b11111111) == 0b11111111) || (sensor_cmp(0b11111111) == 0b01111110)||(sensor_cmp(0b11111111) == 0b01111111)||(sensor_cmp(0b11111111) == 0b11111110))ret = 1;
	//return ret;
	if(check_rightline())
	{
		int i=0;
		while((i++)<5)
		{
			if(check_leftline())
			{
				ret=1;
				return ret;
			}
		}
	}
	else if(check_leftline())
	{
		int i=0;
		while((i++)<5)
		{
			if(check_rightline())
			{
				ret=1;
				return ret;
			}

		}
	}
	else if ((sensor_cmp(0b11111111) == 0b11111111) || (sensor_cmp(0b11111110) == 0b01111110)||(sensor_cmp(0b01111111) == 0b01111111)||(sensor_cmp(0b11111110) == 0b11111110))ret = 1;
	return ret;
}
int check_rightline(void)
{
	int ret = 0;
	if ((sensor_cmp(0b00001111) == 0b00001111) || (sensor_cmp(0b00011111) == 0b00011111))
	{
		ret =1;
	}
	return ret;
}
int check_leftline(void)
{
	int ret = 0;
	if ((sensor_cmp(0b11110000) == 0b11110000) || (sensor_cmp(0b11111000) == 0b11111000))
	{
		ret = 1;
	}
	return ret;
}
int check_dunglai(void)
{
	int ret = 0;
	if ((sensor_cmp(0b01111111) == 0b11111111))
	{
		ret =1;
		
	}
	return ret;
}